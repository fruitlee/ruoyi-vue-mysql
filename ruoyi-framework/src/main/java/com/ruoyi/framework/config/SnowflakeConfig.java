package com.ruoyi.framework.config;

import com.ruoyi.common.utils.SnowflakeWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: LJ
 * @Date: 2021/10/5 18:26
 * @version:
 */
@Slf4j
@Configuration
public class SnowflakeConfig {

    /**
     * 工作机器ID(0~31)
     */
    @Value("${snowflake.workerId}")
    private long workerId;

    /**
     * 数据中心ID(0~31)
     */
    @Value("${snowflake.dataCenterId}")
    private long dataCenterId;

    /**
     *  内外网(0内网1外网)
     */
    @Value("${snowflake.enabled}")
    private String enabled;

    @Bean
    public SnowflakeWorker snowflakeWorker() {
        log.info("获取到雪花算法参数 workerId={}，dataCenterId={},enabled={}", workerId, dataCenterId, enabled);
        return new SnowflakeWorker(workerId, dataCenterId, enabled);
    }
}
