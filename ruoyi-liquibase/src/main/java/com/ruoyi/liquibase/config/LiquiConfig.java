package com.ruoyi.liquibase.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @Description:
 * @Author: LJ
 * @Date: 2021/9/10 10:19
 * @version:
 */
@Configuration
public class LiquiConfig {
    /**
     * 系统模块
     * @param dataSource
     * @return
     */
    @Bean
    public SpringLiquibase sysLiquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        //指定changelog的位置，这里使用的一个master文件引用其他文件的方式
        liquibase.setChangeLog("classpath:liquibase/sys/master.xml");
        liquibase.setContexts("development,test,production");
        liquibase.setShouldRun(true);
        liquibase.setDropFirst(false);
//        liquibase.setResourceLoader(new DefaultResourceLoader());
        // 覆盖Liquibase changelog表名
        liquibase.setDatabaseChangeLogTable("sys_changelog_table");
        liquibase.setDatabaseChangeLogLockTable("sys_changelog_lock_table");
        return liquibase;
    }
//    /**
//     *  其他模块Liquibase
//     */
//    @Bean
//    public SpringLiquibase orderLiquibase(DataSource dataSource) {
//        // TODO: 2021/12/15 需要改
//        SpringLiquibase liquibase = new SpringLiquibase();
//        liquibase.setDataSource(dataSource);
//        liquibase.setChangeLog("classpath:liquibase/order/master.xml");
//        liquibase.setContexts("development,test,production");
//        liquibase.setShouldRun(true);
//        liquibase.setDropFirst(false);
////        liquibase.setResourceLoader(new DefaultResourceLoader());
//        liquibase.setDatabaseChangeLogTable("order_changelog_table");
//        liquibase.setDatabaseChangeLogLockTable("order_changelog_lock_table");
//        return liquibase;
//    }
}
