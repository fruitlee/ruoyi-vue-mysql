package com.ruoyi.web.controller.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

/**
 * @Description: 消息订阅接收haizhenm
 * @Author: LJ
 * @Date: 2021/12/23 15:56
 * @version:
 */
@Slf4j
@Service
public class SubscribeService implements MessageListener {
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String topicType = new String(message.getChannel());
        log.debug("订阅频道:topicType={}，接收数据:message={}", new String(message.getChannel()), new String(message.getBody()));
        switch (topicType) {
            case "":
                ;
            default:
                break;
        }
    }
}
