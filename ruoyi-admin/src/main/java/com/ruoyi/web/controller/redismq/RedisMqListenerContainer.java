package com.ruoyi.web.controller.redismq;

import com.ruoyi.common.core.redis.RedisChannelTopic;
import com.ruoyi.web.controller.test.SubscribeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * @Description:
 * @Author: LJ
 * @Date: 2021/12/23 16:30
 * @version:
 */
@Configuration
public class RedisMqListenerContainer {
    /**
     * redis消息监听器容器
     * 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
     * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
     *
     * @param connectionFactory
     * @return
     */
    //MessageListenerAdapter 表示监听频道的不同订阅者
    @Bean
    @SuppressWarnings(value = {"unchecked", "rawtypes"})
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //订阅多个频道
        container.addMessageListener(listenerAdapter(), new PatternTopic(RedisChannelTopic.TEST));
        return container;
    }

    //表示监听一个频道
    @Bean
    MessageListenerAdapter listenerAdapter() {
        //这个地方 是给messageListenerAdapter 传入一个消息接受的处理器，利用反射的方法调用“SubscribeService ”
        return new MessageListenerAdapter(new SubscribeService());
    }
}
