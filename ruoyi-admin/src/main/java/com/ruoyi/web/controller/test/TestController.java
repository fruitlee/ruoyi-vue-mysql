package com.ruoyi.web.controller.test;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.core.redis.RedisChannelTopic;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

@Api(tags = "测试类")
@RestController
@RequestMapping("/test")
public class TestController extends BaseController {
    @Autowired
    private RedisCache redisCache;

    @Anonymous
    @ApiOperation("测试缓存")
    @GetMapping("/redisCache")
    @Cacheable(value = "testRedisCache", key = "'testId:' + #testId")
    public String list(@RequestParam @ApiParam("测试testId") String testId) {
        SysUser a = new SysUser();
        a.setNickName(testId);
        redisCache.setCacheObject("test", a);
        logger.debug("测试进来 testId={}", testId);
        return testId + "获取成功";
    }

    @Anonymous
    @ApiOperation("测试删除缓存")
    @DeleteMapping("/redisCache")
    public String deleteRedis(@RequestParam @ApiParam("测试testId") String testId) {
        SysUser sysUser = redisCache.getCacheObject("test");
        redisCache.deleteObject("test");
        redisCache.deleteObject("testRedisCache:testId:" + testId);
        logger.debug("测试清除缓存 testId={}", testId);
        return "清除缓存成功";
    }

    @ApiOperation("测试发布消息")
    @GetMapping("/message-publish")
    public void publish(@RequestParam @ApiParam("发布数据") String message) {
        logger.debug("发布消息demomessage={}", message);
        redisCache.convertAndSend(RedisChannelTopic.TEST, message);
    }

    //public static void main(String[] args) {
    //    System.out.println(Inflector.getInstance().pluralize("jobLog").toLowerCase().replace("_", "-"));
    //}

}
