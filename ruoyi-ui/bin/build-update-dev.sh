service=ruoyi

cd /root/git/${service}/ruoyi-ui
git reset --hard origin/master
git pull

cd /root/git/${service}/ruoyi-ui/bin
chmod 755  build-dev.sh

ln -s /root/git/${service}/ruoyi-ui/bin/build-dev.sh /root/deploy/${service}-vue.sh -f
ln -s /root/git/${service}/ruoyi-ui/bin/build-update-dev.sh /root/deploy/auto/${service}-vue.sh -f

cd /root/deploy/auto
chmod 755 ${service}.sh

cd /root/deploy
chmod 755 ${service}.sh
./${service}.sh &


