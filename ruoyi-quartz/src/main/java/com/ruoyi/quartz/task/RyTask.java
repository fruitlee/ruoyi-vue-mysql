package com.ruoyi.quartz.task;

import com.ruoyi.common.utils.TimeDurationLogUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Slf4j
@Component("ryTask")
public class RyTask {
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        TimeDurationLogUtil.execute("执行多参方法", (p) -> {
            log.info("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i);
            return null;
        });

    }

    public void ryParams(String params) {
        TimeDurationLogUtil.execute("执行有参方法：" , (p) -> {
            //"方法"
            log.info("执行有参方法： params ={}", params);
            return null;
        });
    }

    public void ryNoParams() {
        TimeDurationLogUtil.execute("执行无参方法", (p) -> {
            //"方法"
            return null;
        });

    }
}
