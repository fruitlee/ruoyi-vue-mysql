package com.ruoyi.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.function.Function;

public class TimeDurationLogUtil {
    private static final Logger log = LoggerFactory.getLogger(TimeDurationLogUtil.class);

    public static void execute(String text, Function<Void, Void> func) {
        Date date = new Date();
        log.info("{} - 开始时间: -> {}", text, DateUtils.getTime());
        try {
            func.apply(null);
            log.info("{} - 完成时间: -> {} - [ 耗时 {} 秒 ]", text, DateUtils.getTime(), DateUtils.calLastedTime(date, new Date()));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("{} - 异常时间: -> {} 。异常信息：{}", text, DateUtils.getTime(), e.getMessage());
        }
    }
}
