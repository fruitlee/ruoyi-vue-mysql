package com.ruoyi.common.utils;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import java.util.Set;

/**
 * @Description:
 * @Author: LJ
 * @Date: 2021/12/14 16:19
 * @version:
 */
public class StringJsonUtils {


    public static void main(String[] args) {
        String str = "[\" 1\" ,\"2  \",\"4  \"]";
//        str = "{\"name\":\"张三  \"}";
//        str = "{\"name\":  {\"first_name\": \"张  \",\"last_name\":\"  三  \"}}";
        str = "{\n" +
                "    \"searchValue\": null,\n" +
                "    \"createBy\": \"admin\",\n" +
                "    \"createTime\": \"2021-11-24 08:50:39\",\n" +
                "    \"updateBy\": 'null',\n" +
                "    \"updateTime\": 'undefined',\n" +
                "    \"remark\": \"测试员\",\n" +
                "    \"delState\": null,\n" +
                "    \"params\": {\n" +
                "        \n" +
                "    },\n" +
                "    \"userId\": 2,\n" +
                "    \"deptId\": 105,\n" +
                "    \"userName\": \"ry\",\n" +
                "    \"nickName\": \"若依   \",\n" +
                "    \"email\": \"ry@qq.com\",\n" +
                "    \"phonenumber\": \"15666666666\",\n" +
                "    \"sex\": \"1\",\n" +
                "    \"avatar\": \"\",\n" +
                "    \"salt\": null,\n" +
                "    \"status\": \"0\",\n" +
                "    \"delFlag\": \"0\",\n" +
                "    \"loginIp\": \"127.0.0.1\",\n" +
                "    \"loginDate\": \"2021-11-24T08:50:39.000+08:00\",\n" +
                "    \"dept\": {\n" +
                "        \"searchValue\": null,\n" +
                "        \"createBy\": null,\n" +
                "        \"createTime\": null,\n" +
                "        \"updateBy\": null,\n" +
                "        \"updateTime\": null,\n" +
                "        \"remark\": null,\n" +
                "        \"delState\": null,\n" +
                "        \"params\": {\n" +
                "            \n" +
                "        },\n" +
                "        \"deptId\": 105,\n" +
                "        \"parentId\": 101,\n" +
                "        \"ancestors\": null,\n" +
                "        \"deptName\": \"测试部门\",\n" +
                "        \"orderNum\": \"3\",\n" +
                "        \"leader\": \"若依\",\n" +
                "        \"phone\": null,\n" +
                "        \"email\": null,\n" +
                "        \"status\": \"0\",\n" +
                "        \"delFlag\": null,\n" +
                "        \"parentName\": null,\n" +
                "        \"children\": [\n" +
                "            \n" +
                "        ]\n" +
                "    },\n" +
                "    \"roles\": [\n" +
                "        {\n" +
                "            \"searchValue\": null,\n" +
                "            \"createBy\": null,\n" +
                "            \"createTime\": null,\n" +
                "            \"updateBy\": null,\n" +
                "            \"updateTime\": null,\n" +
                "            \"remark\": null,\n" +
                "            \"delState\": null,\n" +
                "            \"params\": {\n" +
                "                \n" +
                "            },\n" +
                "            \"roleId\": 2,\n" +
                "            \"roleName\": \"普通管理员\",\n" +
                "            \"roleKey\": \"common\",\n" +
                "            \"roleSort\": \"2\",\n" +
                "            \"dataScope\": \"2\",\n" +
                "            \"menuCheckStrictly\": false,\n" +
                "            \"deptCheckStrictly\": false,\n" +
                "            \"status\": \"0\",\n" +
                "            \"delFlag\": null,\n" +
                "            \"flag\": false,\n" +
                "            \"menuIds\": null,\n" +
                "            \"deptIds\": null,\n" +
                "            \"admin\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"searchValue\": null,\n" +
                "            \"createBy\": null,\n" +
                "            \"createTime\": null,\n" +
                "            \"updateBy\": null,\n" +
                "            \"updateTime\": null,\n" +
                "            \"remark\": null,\n" +
                "            \"delState\": null,\n" +
                "            \"params\": {\n" +
                "                \n" +
                "            },\n" +
                "            \"roleId\": 3,\n" +
                "            \"roleName\": \"注册用户\",\n" +
                "            \"roleKey\": \"register_user\",\n" +
                "            \"roleSort\": \"3\",\n" +
                "            \"dataScope\": \"3\",\n" +
                "            \"menuCheckStrictly\": false,\n" +
                "            \"deptCheckStrictly\": false,\n" +
                "            \"status\": \"0\",\n" +
                "            \"delFlag\": null,\n" +
                "            \"flag\": false,\n" +
                "            \"menuIds\": null,\n" +
                "            \"deptIds\": null,\n" +
                "            \"admin\": false\n" +
                "        }\n" +
                "    ],\n" +
                "    \"roleIds\": [\n" +
                "        2,\n" +
                "        3\n" +
                "    ],\n" +
                "    \"postIds\": [\n" +
                "        2\n" +
                "    ],\n" +
                "    \"roleId\": null,\n" +
                "    \"zaUser\": null,\n" +
                "    \"isPwd\": null,\n" +
                "    \"admin\": false,\n" +
                "    \"password\": \"\"\n" +
                "}";

        Object jsonObj = JSON.parse(str);
        if (jsonObj instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) jsonObj;
            parseJsonObject(jsonObject);
        } else if (jsonObj instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) jsonObj;
            parseJSONArray(jsonArray);
        }
        System.out.println(jsonObj);
    }

    public static void parseJsonObject(JSONObject jsonObject) {
        Set<String> keySet = jsonObject.keySet();
        for (String key : keySet) {
            parseObject(jsonObject, key);
        }
    }

    public static void parseJSONArray(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.isEmpty()) {
            return;
        }
        for (int i = 0; i < jsonArray.size(); i++) {
            parseArray(jsonArray, i);
        }

    }

    public static void parseObject(JSONObject jsonObject, String key) {
        Object keyObj = jsonObject.get(key);
        if (keyObj == null) {
            return;
        }
        if (keyObj instanceof String) {
            if ("".equals(((String) keyObj).trim()) || "null".equalsIgnoreCase(((String) keyObj)) || "undefined".equalsIgnoreCase(((String) keyObj))) {
                jsonObject.put(key, null);
                return;
            }
            jsonObject.put(key, ((String) keyObj).trim());
        } else if (keyObj instanceof JSONObject) {
            //解析json 对象
            parseJsonObject(((JSONObject) keyObj));
        } else if (keyObj instanceof JSONArray) {
            //解析json 数组
            parseJSONArray(((JSONArray) keyObj));
        }

    }

    public static void parseArray(JSONArray jsonArray, int index) {
        Object keyObj = jsonArray.get(index);
        if (keyObj == null) {
            return;
        }
        if (keyObj instanceof String) {
            if ("".equals(((String) keyObj).trim()) || "null".equalsIgnoreCase(((String) keyObj)) || "undefined".equalsIgnoreCase(((String) keyObj))) {
                jsonArray.set(index, null);
                return;
            }
            jsonArray.set(index, ((String) keyObj).trim());
        } else if (keyObj instanceof JSONObject) {
            //解析json 对象
            parseJsonObject(((JSONObject) keyObj));
        } else if (keyObj instanceof JSONArray) {
            //解析json 数组
            parseJSONArray(((JSONArray) keyObj));
        }
    }


}
