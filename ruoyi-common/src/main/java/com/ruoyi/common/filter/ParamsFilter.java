package com.ruoyi.common.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * @Description: 过滤器使用filter拦截参数去掉两端的空格
 * @Author: LJ
 * @Date: 2018-06-12 17:04
 * @version:
 */
public class ParamsFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {

    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        //去除空格切空处理
        ParameterRequestWrapper requestWrapper = new ParameterRequestWrapper(httpServletRequest);
        filterChain.doFilter(requestWrapper, servletResponse);

    }

    @Override
    public void destroy() {

    }
}
