package com.ruoyi.common.core.page;

import com.github.pagehelper.PageInfo;
import com.ruoyi.common.constant.HttpStatus;

import java.util.List;

/**
 * @Description:
 * @Author: LJ
 * @Date: 2021/10/19 8:26
 * @version:
 */
public class PageUtil {
    /**
     * 分页返回参数
     *
     * @param list
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static <T> TableDataInfo<T> getDataTable(List<T> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 分页返回参数
     *
     * @param list
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static <T> TableDataInfo<T> getDataTable(List<T> list, long total) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(total);
        return rspData;
    }
}
