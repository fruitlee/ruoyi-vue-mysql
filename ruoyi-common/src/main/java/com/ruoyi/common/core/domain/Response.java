package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 【Response返回实体类】
 *
 * @author JuniorRay
 * @date 2020-11-12
 */
@ApiModel(description = "结果返回类")
@Data
public class Response<T> {
    @ApiModelProperty(value = "状态码")
    private Integer code;

    @ApiModelProperty(value = "返回内容")
    private String msg;

    @ApiModelProperty(value = "数据对象")
    private T data;

    /**
     * 返回成功数据
     *
     * @param data
     */
    private Response(T data) {
        this.code = HttpStatus.SUCCESS;
        this.msg = "操作成功";
        this.data = data;
    }

//    private Response(int code, String msg, T data) {
//        this.code = code;
//        this.msg = msg;
//        this.data = data;
//    }

//    /**
//     * 成功时调用
//     *
//     * @param
//     * @param <T>
//     * @return
//     */
//    public static <T> Response<T> success() {
//        return success(null);
//    }
//
//    /**
//     * 成功时调用
//     *
//     * @param data
//     * @param <T>
//     * @return
//     */
    public static <T> Response<T> success(T data) {
        return new Response<T>(data);
    }
//
//    public static <T> Response<T> success(String msg) {
//        return success(msg, null);
//    }
//
//    public static <T> Response<T> success(String msg, T data) {
//        return new Response<T>(HttpStatus.SUCCESS, msg, data);
//    }
//
//    /**
//     * 失败时调用
//     *
//     * @param
//     * @param <T>
//     * @return
//     */
//    public static <T> Response error() {
//        return error("操作失败");
//    }
//
//    /**
//     * 失败时调用
//     *
//     * @param msg 返回内容
//     * @param <T>
//     * @return
//     */
//    public static <T> Response error(String msg) {
//        return error(HttpStatus.ERROR, msg);
//    }
//
//    public static <T> Response error(int code, String msg) {
//        return new Response<T>(HttpStatus.ERROR, msg, null);
//    }
}
