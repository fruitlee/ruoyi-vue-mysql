package com.ruoyi.common.core.redis;

import com.ruoyi.common.utils.ToolUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * spring redis 工具类
 *
 * @author ruoyi
 **/
@Slf4j
@SuppressWarnings(value = {"unchecked", "rawtypes"})
@Component
public class RedisCache {
    @Autowired
    public RedisTemplate redisTemplate;
    @Autowired
    public StringRedisTemplate stringRedisTemplate;

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key   缓存的键值
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key      缓存的键值
     * @param value    缓存的值
     * @param timeout  时间
     * @param timeUnit 时间颗粒度
     */
    public <T> void setCacheObject(final String key, final T value, final Integer timeout, final TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(final String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @param unit    时间单位
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    /**
     * 获取有效时间
     *
     * @param key Redis键
     * @return 有效时间
     */
    public long getExpire(final String key)
    {
        return redisTemplate.getExpire(key);
    }
    /**
     * 判断 key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final String key) {
        ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key);
    }

    /**
     * 删除单个对象
     *
     * @param key
     */
    public boolean deleteObject(final String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 删除集合对象
     *
     * @param collection 多个对象
     * @return
     */
    public boolean deleteObject(final Collection collection) {
        return redisTemplate.delete(collection) > 0;
    }

    /**
     * 缓存List数据
     *
     * @param key      缓存的键值
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    public <T> long setCacheList(final String key, final List<T> dataList) {
        Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
        return count == null ? 0 : count;
    }

    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(final String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    /**
     * 缓存Set
     *
     * @param key     缓存键值
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> BoundSetOperations<String, T> setCacheSet(final String key, final Set<T> dataSet) {
        BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
        Iterator<T> it = dataSet.iterator();
        while (it.hasNext()) {
            setOperation.add(it.next());
        }
        return setOperation;
    }

    /**
     * 获得缓存的set
     *
     * @param key
     * @return
     */
    public <T> Set<T> getCacheSet(final String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     */
    public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
        if (dataMap != null) {
            redisTemplate.opsForHash().putAll(key, dataMap);
        }
    }

    /**
     * 获得缓存的Map
     *
     * @param key
     * @return
     */
    public <T> Map<String, T> getCacheMap(final String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 往Hash中存入数据
     *
     * @param key   Redis键
     * @param hKey  Hash键
     * @param value 值
     */
    public <T> void setCacheMapValue(final String key, final String hKey, final T value) {
        redisTemplate.opsForHash().put(key, hKey, value);
    }

    /**
     * 获取Hash中的数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    public <T> T getCacheMapValue(final String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
        return opsForHash.get(key, hKey);
    }

    /**
     * 删除Hash中的数据
     *
     * @param key
     * @param hkey
     */
    public void delCacheMapValue(final String key, final String hkey) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.delete(key, hkey);
    }

    /**
     * 获取多个Hash中的数据
     *
     * @param key   Redis键
     * @param hKeys Hash键集合
     * @return Hash对象集合
     */
    public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hKeys) {
        return redisTemplate.opsForHash().multiGet(key, hKeys);
    }

    /**
     * 获得缓存的基本对象列表
     *
     * @param pattern 字符串前缀
     * @return 对象列表
     */
    public Collection<String> keys(final String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 删除Hash中的某条数据
     *
     * @param key Redis键
     * @param hKey Hash键
     * @return 是否成功
     */
    public boolean deleteCacheMapValue(final String key, final String hKey)
    {
        return Boolean.TRUE.equals(redisTemplate.opsForHash().delete(key, hKey));
    }

    /**
     * ============================分布式锁=============================
     */
    private static final long DEFAULT_EXPIRE_MILLI_SECONDS = 30 * 60 * 1000; // 30分钟
    private static final int LOCK_TIMEOUT = 10000; // 10秒
    private static final int LOCK_SLEEP = 100; // 100毫秒
    private static final int LOCK_LOOPS = 500;
    private static final String REDIS_KEY_FOR_LOCK_PREFIX = "REDIS_INTERNAL_LOCK_";
    private static final String REDIS_KEY_FOR_SET_PREFIX = "REDIS_CACHE_KEYS_";
    private static final String REDIS_KEY_FOR_CUSTOM_EXPIRES = "MESH_CUSTOM_EXPIRES";
    private long timeDiff = 0;
    private long lastGetTime = 0;

    private void signCustomExpires(String key) {
        redisTemplate.opsForSet().add(REDIS_KEY_FOR_CUSTOM_EXPIRES, key);
    }

    private void expire(final String key, Long expireMilliSeconds) {
        if (ToolUtil.isEmpty(key) || null == expireMilliSeconds || expireMilliSeconds < 0) return;
        if (DEFAULT_EXPIRE_MILLI_SECONDS != expireMilliSeconds) signCustomExpires(key);
        redisTemplate.expire(key, expireMilliSeconds, TimeUnit.MILLISECONDS);
    }

    private long time() {
        long now = System.currentTimeMillis() + timeDiff;
        if (now - lastGetTime > 60000) {  // 每分钟从redis获取时间
//            RedisServerCommands::time
            Long t = (Long) redisTemplate.execute(RedisConnection::time, false);
            log.debug("获取redis时间：{}", t);
            if (t != null) {
                timeDiff = t - now;
                now = t;
            }
            lastGetTime = now;
        }
        return now;
    }

    /***
     * 加锁
     * @param key 需要被加锁的KEY
     * @return 返回锁ID，如果返回0则给示加锁失败，大于0表示加锁成功
     */
    private long lock(String key) {
        return lock(key, LOCK_TIMEOUT);
    }

    private long lock(String key, int timeoutMilliSeconds) {
        int loops = 0;
        String lockKey = REDIS_KEY_FOR_LOCK_PREFIX + key;
        while (true) {
            long lockId = time();
            long oldLockId = ToolUtil.isEmpty(getCacheObject(lockKey)) ? 0L : getCacheObject(lockKey);
            if (lockId - oldLockId > timeoutMilliSeconds) deleteObject(lockKey);  // 锁超时，作废

//            Boolean isLock = redisTemplate.opsForValue().setIfAbsent(lockKey, String.valueOf(lockId));
            Boolean isLock = redisTemplate.opsForValue().setIfAbsent(lockKey, lockId, timeoutMilliSeconds, TimeUnit.MILLISECONDS);
            if (isLock != null && isLock) {
                log.debug("成功获取redis锁：key={}, lockId={}", key, lockId);
                return lockId;
            }
            loops++;
            if (loops > LOCK_LOOPS) {
                log.error("Redis加锁失败，等待时间超限！");
                throw new RuntimeException("Redis加锁失败，等待时间超限！");
            }

            try {
                Thread.sleep(LOCK_SLEEP);
            } catch (Exception ex) {
                return 0;
            }
        }
    }

    /***
     * 解锁
     * @param key
     * @param lockId
     * @return
     */
    private void unlock(String key, long lockId) {
        if (lockId == 0) return;
        try {
            String lockKey = REDIS_KEY_FOR_LOCK_PREFIX + key;
            long currentValue = getCacheObject(lockKey);
            if (ToolUtil.isNotEmpty(currentValue) && currentValue == lockId) {
                deleteObject(lockKey);
            }
            log.debug("成功解除redis锁：key={}, lockId={}", key, lockId);
        } catch (Exception e) {
            log.error("解锁异常");
        }
    }

    /**
     * reids分布式锁
     *
     * @param key
     * @param executeFun
     * @param <R>
     * @return
     */
    public <R> R lockExecute(String key, Function<Long, ? extends R> executeFun) {
        return lockExecute(key, executeFun, LOCK_TIMEOUT);
    }

    public <R> R lockExecute(String key, Function<Long, ? extends R> executeFun, int timeoutMilliSeconds) {
        long lockId = lock(key, timeoutMilliSeconds);
        try {
            return executeFun.apply(lockId);
        } finally {
            unlock(key, lockId);
        }
    }

    /**
     * 获取所有的key(带特定前缀的)
     */
    public Set<String> getListKey(String prefix) {
        Set<String> keys = redisTemplate.keys(prefix + "*");
        return keys;
    }


    /**
     * 消息发布
     *
     * @param channel ChannelTopic消息主题
     * @param message 发送的数据
     */
    public void convertAndSend(String channel, String message) {
        stringRedisTemplate.convertAndSend(channel, message);
    }
}
