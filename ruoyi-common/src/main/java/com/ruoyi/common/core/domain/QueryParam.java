package com.ruoyi.common.core.domain;

import com.github.pagehelper.PageHelper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.sql.SqlUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author ruoyi
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("通用查询字段")
public class QueryParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "页数(默认1)")
    private Integer pageNum = 1;
    @ApiModelProperty(value = "每页显示条数(默认10)")
    private Integer pageSize = 10;
    @ApiModelProperty(value = "排序列")
    private String orderByColumn;
    @ApiModelProperty(value = "排序的方向desc或者asc")
    private String isAsc = "asc";
    @ApiModelProperty(value = "分页参数合理化(默认false超过返回空/true超过返回最后一页)")
    private Boolean reasonable = false;

    /**
     * 设置不分页
     */
    public void unpaged() {
        pageNum = null;
        pageSize = null;
        String orderBy = SqlUtil.escapeOrderBySql(getOrderBy());
        PageHelper.orderBy(orderBy);
    }
    private String getOrderBy() {
        if (StringUtils.isEmpty(orderByColumn)) {
            return "";
        }
        return StringUtils.toUnderScoreCase(orderByColumn) + " " + isAsc;
    }
}
