#!/bin/bash
binDir=$(cd $(dirname $0); pwd)
cd ${binDir}/..
mvn clean package -Pprod -Dmaven.test.skip=true
