service=ruoyi

cd /root/git/${service}
git reset --hard origin/master
git pull

jps -l | grep -E "${service}" | cut -d " " -f 1 | xargs kill
ps -aux | grep "${service}.sh" | awk '{print $2}' | xargs kill

cd /root/git/${service}/bin
chmod 755 auto-update-dev.sh package-prod.sh start-dev.sh
./package-prod.sh

ln -s /root/git/${service}/ruoyi-admin/target/ruoyi-admin.jar /root/deploy/${service}.jar -f
ln -s /root/git/${service}/bin/start-dev.sh /root/deploy/${service}.sh -f
ln -s /root/git/${service}/bin/auto-update-dev.sh /root/deploy/auto/${service}.sh -f

cd /root/deploy/auto
chmod 755 ${service}.sh

cd /root/deploy
chmod 755 ${service}.sh
./${service}.sh &

tail -f /root/deploy/out/${service}.out
