#!/bin/bash
# ---------------------------------------------------------
echo -e "keep process usage: "
echo -e "$ nohup ./<name>.sh & "
# ---------------------------------------------------------

jar_file_name=ruoyi
profile_active=prod,prod-druid
jvm_args="-Xms1024m -Xmx2048m -XX:+ScavengeBeforeFullGC -XX:+CMSScavengeBeforeRemark"

out_logs=true

# ---------------------------------------------------------
# ---------------------------------------------------------

binDir=$(cd $(dirname $0); pwd)
cd ${binDir}
jar_file=${binDir}/${jar_file_name}.jar
if [ ! -e "${jar_file}" ]
then
  jar_file=${binDir}/../target/*.jar
  cd ${binDir}/..
fi



while :
do
#echo -e "starting now ..."
isRunning=$(ps -ef |grep "${jar_file}" |grep -v "grep")
if [ -z "$isRunning" ]
then
  #echo -e "starting now -->  java -jar ${jar_file} --spring.profiles.active=${profile_active}"
  if [ "${out_logs}" = "true" ]
  then
    nohup java ${jvm_args} -jar ${jar_file} --spring.profiles.active=${profile_active} > out/${jar_file_name}.out &
  else
    nohup java ${jvm_args} -jar ${jar_file} --spring.profiles.active=${profile_active}
  fi
fi

sleep 30
done