service=ruoyi

jps -l | grep -E "${service}" | cut -d " " -f 1 | xargs kill
ps -aux | grep "${service}.sh" | awk '{print $2}' | xargs kill
