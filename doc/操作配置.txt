linux自动脚本部署:
1 git拉取保存账户：
根目录：
创建： touch .git-credentials

编辑： vim .git-credentials 点击i编辑 esc :x保存退出

编辑内容 ： http(s)://{你的用户名}:{你的密码}@项目地址

git config --global credential.helper store
2 创建执行部署文件夹
 
执行 mkdir.sh 创建文件夹

auto-update-dev.sh 拉取代码 杀掉当前进程 并打包部署 dev环境
auto-update-prod.sh 拉取代码 杀掉当前进程 并打包部署 prod环境
kill.sh 杀进程
package-prod.sh 打包
start-dev.sh 部署 dev
start-prod.sh 部署 prod